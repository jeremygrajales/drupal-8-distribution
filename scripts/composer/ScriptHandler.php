<?php

/**
 * @file
 * Contains \DrupalProject\composer\ScriptHandler
 */

namespace DrupalProject\composer;

use Composer\Script\Event;
use Composer\Semver\Comparator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

class ScriptHandler {

  protected static function getSymlinkConfig() {
    $symlinkConfigFile = __DIR__ . '/symlinks.json';
    if(file_exists($symlinkConfigFile)) {
      $json = file_get_contents($symlinkConfigFile);
      $json = json_decode($json);
      return $json ? $json : false;
    }
    return false;
  }
  
  protected static function getSymlinkFiles() {
    $return = self::getSymlinkConfig();
    return $return ? $return->files : array();
  }
  
  protected static function getSymlinkPath() {
    $return = self::getSymlinkConfig();
    return $return ? $return->path : false;
  }
    
  protected static function getDrupalRoot($project_root) {
    return $project_root . '/web';
  }
  
  public static function checkSymlinks(Event $event) {
    $drupalRoot = static::getDrupalRoot(getcwd());
    if($event->isDevMode()) {
      foreach(self::getSymlinkFiles() as $file) {
        $symlink = $drupalRoot . '/' . $file;
        if(file_exists($symlink) && is_link($symlink)) {
          $event->getIO()->writeError("\n<error>Core symlink found $symlink\nRemove core symlink before proceeding.\nRun `composer remove-symlinks`\nOr run `composer update --no-dev` to skip core updates<error>");
          exit;
        }
      }
    }
  }
  
  public static function removeSymlinks(Event $event) {
    $drupalRoot = static::getDrupalRoot(getcwd());
    foreach(self::getSymlinkFiles() as $file) {
      $symlink = $drupalRoot . '/' . $file;
      if(file_exists($symlink) && is_link($symlink)) {
        $event->getIO()->write('Removing symlink: ' . $symlink);
        unlink($symlink);
      }
    }
  }

  public static function createSymlinks(Event $event) {
    $drupalRoot = static::getDrupalRoot(getcwd());
    
    $corePath = self::getSymlinkPath();
    print "|";
    print_r($corePath);
    print "|";
    
    if(!$corePath) {
      $event->getIO()->writeError("\n<error>Please provide a valid 'path' in /scripts/composer/symlinks.json (use /scripts/composer/example.symlinks.json as a template).</error>");
      exit;
    }
    $event->getIO()->write('Identified core path as: ' . $corePath);  
    
    if(!self::isValidCore($corePath)) {
      $event->getIO()->writeError("\n<error>Provided 'path' is not a valid Drupal directory in symlinks.json config</error>");
      exit;
    }
    
    foreach(self::getSymlinkFiles() as $file) {
      $link = $drupalRoot . '/' . $file;
      if(!is_link($link) && is_dir($link)) {
        $event->getIO()->writeError("\n<error>Please delete $link before continuing.");
        exit;
      }
    }
    
    foreach(self::getSymlinkFiles() as $file) {
      $link = $drupalRoot . '/' . $file;
      $target = $corePath . '/' . $file;
      if (file_exists($target)) {
        $event->getIO()->write('Writing symlink: ' . $link . ' > ' . $target);
        !file_exists($link) ? : unlink($link);
        symlink($target, $link);
      }
    }
  }
  
  public static function isValidCore($path) {
    foreach(self::getSymlinkFiles() as $file) {
      $origin = $path . '/' . $file;
      if (!file_exists($origin)) {
        return false;
      }
    }
    return true;
  }

  public static function createRequiredFiles(Event $event) {
    $fs = new Filesystem();
    $root = static::getDrupalRoot(getcwd());

    $dirs = [
      'modules',
      'profiles',
      'themes',
    ];

    // Required for unit testing
    foreach ($dirs as $dir) {
      if (!$fs->exists($root . '/'. $dir)) {
        $fs->mkdir($root . '/'. $dir);
        $fs->touch($root . '/'. $dir . '/.gitkeep');
      }
    }

    // Prepare the settings file for installation
    if (!$fs->exists($root . '/sites/default/settings.php') and $fs->exists($root . '/sites/default/default.settings.php')) {
      $fs->copy($root . '/sites/default/default.settings.php', $root . '/sites/default/settings.php');
      $fs->chmod($root . '/sites/default/settings.php', 0666);
      $event->getIO()->write("Create a sites/default/settings.php file with chmod 0666");
    }

    // Prepare the services file for installation
    if (!$fs->exists($root . '/sites/default/services.yml') and $fs->exists($root . '/sites/default/default.services.yml')) {
      $fs->copy($root . '/sites/default/default.services.yml', $root . '/sites/default/services.yml');
      $fs->chmod($root . '/sites/default/services.yml', 0666);
      $event->getIO()->write("Create a sites/default/services.yml file with chmod 0666");
    }

    // Create the files directory with chmod 0777
    if (!$fs->exists($root . '/sites/default/files')) {
      $oldmask = umask(0);
      $fs->mkdir($root . '/sites/default/files', 0777);
      umask($oldmask);
      $event->getIO()->write("Create a sites/default/files directory with chmod 0777");
    }
  }

  /**
   * Checks if the installed version of Composer is compatible.
   *
   * Composer 1.0.0 and higher consider a `composer install` without having a
   * lock file present as equal to `composer update`. We do not ship with a lock
   * file to avoid merge conflicts downstream, meaning that if a project is
   * installed with an older version of Composer the scaffolding of Drupal will
   * not be triggered. We check this here instead of in drupal-scaffold to be
   * able to give immediate feedback to the end user, rather than failing the
   * installation after going through the lengthy process of compiling and
   * downloading the Composer dependencies.
   *
   * @see https://github.com/composer/composer/pull/5035
   */
  public static function checkComposerVersion(Event $event) {
    $composer = $event->getComposer();
    $io = $event->getIO();

    $version = $composer::VERSION;

    // The dev-channel of composer uses the git revision as version number,
    // try to the branch alias instead.
    if (preg_match('/^[0-9a-f]{40}$/i', $version)) {
      $version = $composer::BRANCH_ALIAS_VERSION;
    }

    // If Composer is installed through git we have no easy way to determine if
    // it is new enough, just display a warning.
    if ($version === '@package_version@' || $version === '@package_branch_alias_version@') {
      $io->writeError('<warning>You are running a development version of Composer. If you experience problems, please update Composer to the latest stable version.</warning>');
    }
    elseif (Comparator::lessThan($version, '1.0.0')) {
      $io->writeError('<error>Drupal-project requires Composer version 1.0.0 or higher. Please update your Composer before continuing</error>.');
      exit(1);
    }
  }

}
