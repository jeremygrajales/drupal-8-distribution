#!/bin/sh

mkdir -p ~/.ssh
umask 077
if [ ! -z "$REMOTE_HOST_DEV" ] && [ ! -z "$USER_DEV" ] && [ ! -z "$SSH_KEY_DEV" ]; then
    ssh-keyscan -t rsa $REMOTE_HOST_DEV >> ~/.ssh/known_hosts
    echo $SSH_KEY_DEV | base64 -d > ~/.ssh/${USER_DEV}@${REMOTE_HOST_DEV}
    echo -e "Host ${REMOTE_HOST_DEV}\n  Hostname ${REMOTE_HOST_DEV}\n  User ${USER_DEV}\n  IdentityFile ~/.ssh/${USER_DEV}@${REMOTE_HOST_DEV}" >> ~/.ssh/config
fi
if [ ! -z "$REMOTE_HOST_STAGE" ] && [ ! -z "$USER_STAGE" ] && [ ! -z "$SSH_KEY_STAGE" ]; then
    ssh-keyscan -t rsa $REMOTE_HOST_STAGE >> ~/.ssh/known_hosts
    echo $SSH_KEY_STAGE | base64 -d > ~/.ssh/${USER_STAGE}@${REMOTE_HOST_STAGE}
    echo -e "Host ${REMOTE_HOST_STAGE}\n  Hostname ${REMOTE_HOST_STAGE}\n  User ${USER_STAGE}\n  IdentityFile ~/.ssh/${USER_STAGE}@${REMOTE_HOST_STAGE}" >> ~/.ssh/config
fi
if [ ! -z "$REMOTE_HOST_PROD" ] && [ ! -z "$USER_PROD" ] && [ ! -z "$SSH_KEY_PROD" ]; then
    ssh-keyscan -t rsa $REMOTE_HOST_PROD >> ~/.ssh/known_hosts 
    echo $SSH_KEY_PROD | base64 -d > ~/.ssh/${USER_PROD}@${REMOTE_HOST_PROD}
    echo -e "Host ${REMOTE_HOST_PROD}\n  Hostname ${REMOTE_HOST_PROD}\n  User ${USER_PROD}\n  IdentityFile ~/.ssh/${USER_PROD}@${REMOTE_HOST_PROD}" >> ~/.ssh/config
fi

composer install --no-dev
mkdir target
mv config target
mv web target
tar -cvzf target.tar.gz target
ls -al
rsync target.tar.gz ${REMOTE_HOST_DEV}:/var/www/html/receive