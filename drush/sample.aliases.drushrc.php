<?php

// // Define the local environment.
// $aliases['local'] = array(
// 	'root' => __DIR__ . '/..',
// 	'path-aliases' => array(
// 		'%dump-dir' => '/tmp',
// 		'%files' => 'sites/default/files',
// 		'command-specific' => array (
// 			'sql-sync' => array (
// 				'simulate' => '0',
// 			),
// 		),
// 	),
// );
// 
// // Define the development environment.
// $aliases['dev'] = array(
// 	'parent' => '@local',
// 	'remote-host' => 'example.com',
// 	'uri'  => 'example.com',
// 	'root' => '/var/www/html/example.com/web'
// );
// 
// // Define the staging environment.
// $aliases['stage'] = array(
// 	'parent' => '@dev',
// 	'remote-host' => 'staging.example.com',
// 	'uri'  => 'staging.example.com',
// 	'path-aliases' => array(
// 		'command-specific' => array (
// 			'sql-sync' => array (
// 				'simulate' => '1',
// 			),
// 		),
// 	),
// );
// 
// // Define the production environment.
// $aliases['prod'] = array(
// 	'parent' => '@stage',
// 	'remote-host' => 'www.example.com',
// 	'uri'  => 'www.example.com'
// );